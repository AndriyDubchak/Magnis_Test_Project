  //add starting field for areas

  var field = document.getElementById("field");
  var fieldWidth = field.clientWidth;
  var fieldHeight = window.innerHeight;
  var areasField= document.createElement("canvas");
  areasField.width = fieldWidth;
  areasField.height = 0.98*fieldHeight;
  areasField.id = "mainCanvas";
  areasField.className = "areaCanvas";

   var ctx = areasField.getContext("2d");
   ctx.textAlign = "center";

   var opacity = 0;
   var drawAreasFieldText = setInterval(function() {
  
   ctx.globalAlpha = opacity;
   ctx.fillStyle = "#666666";
   ctx.font = "50px Arial";
   ctx.fillText("Areas' field",fieldWidth/2,fieldHeight/2-40);
   ctx.fillStyle = "#999999";
   ctx.font = "40px Arial";
   ctx.fillText("Let's start from adding new area",fieldWidth/2,fieldHeight/2 + 40);
   if(opacity >= 0.225) {
      clearInterval(drawAreasFieldText);
   }

   opacity+=0.01;
   }, 100);





   
  //areasField.id = "c";

  field.appendChild(areasField);

  var dinamicField = areasField.getContext("2d");
  
//add elements
var listOfAreas = [];
var listOfTables = [];
var indexOfTable = 0;
var i = 0; //index of Area
function addArea(){
   var areaName = document.getElementById("nameOfArea").value;
   var areaWidthFromUser = document.getElementById("widthOfArea").value;
   var areaHeightFromUser = document.getElementById("heightOfArea").value;

   if(areaName == "" || areaWidthFromUser == "" || areaHeightFromUser == ""){
      $('#notCompletedFields').modal('show');
   }
   else{
   if(areaWidthFromUser <= 0 || areaHeightFromUser <= 0){
      $('#minusOrZeroValue').modal('show');
   }
   else{


   var areaObject = {
      name : areaName,
      width : areaWidthFromUser,
      height : areaHeightFromUser,
      x : -1,
      y : -1,
      tables : []
   };
   listOfAreas.push(areaObject);
   
   var dropdownTables = document.getElementById("areaListForTabels");
   var elementForDropdown = document.createElement("li");
   elementForDropdown.role = "presentation";
   elementForDropdown.innerHTML = "<a role=\"menuitem\" id=dropdawnAreas" + i + "\" class=\"btn\" onclick=\"chooseAreaForTable(" + i + ")\">" + areaObject.name + "</a>";
   dropdownTables.appendChild(elementForDropdown);

   var areaPanel = document.getElementById("areasPanel");

   var newAreaPanel = document.createElement("div");
   var panelBody = document.createElement("div");
   var panelHeading = document.createElement("div");

   newAreaPanel.className = "panel panel-default";
   panelBody.className = "panel-body";
   panelBody.id = "containerForArea" + areaObject.name; 
   panelHeading.className = "panel-heading";

   panelHeading.innerHTML = "<h4>" + areaObject.name + "</h4>";


   newAreaPanel.appendChild(panelHeading);
   newAreaPanel.appendChild(panelBody);

   areasPanel.appendChild(newAreaPanel);

   document.getElementById("addTableCollapse").style = "";

   drawTables();//and areas
   i++;
  }  
 }
}
  function chooseAreaForTable(indexOfArea){
   document.getElementById("areaNameForTable").value = listOfAreas[indexOfArea].name;
}
  
var scale = 1;
function drawAreas(){
   var areasHeight = 0;
   var areasWidth = 0;
   var objIndex = 0
   for(objIndex = 0; objIndex < listOfAreas.length; objIndex++)
   {
      areasWidth += Number(listOfAreas[objIndex].width);
      if(listOfAreas[objIndex].height > areasHeight){
         areasHeight = Number(listOfAreas[objIndex].height);
      }
   }
   var spaces = 0.01*fieldWidth;
   var numberOfspaces = listOfAreas.length + 1;
   var freePixelsWidth = fieldWidth - (spaces*numberOfspaces);
   var freePixelsHeight = 0.98*fieldHeight;
   scale = freePixelsWidth/areasWidth; 
   if(areasHeight*scale > freePixelsHeight){
      scale = freePixelsHeight/areasHeight;
   }
   
   ctx.fillStyle = "#666666";
   ctx.globalAlpha = 1;

   var lastSpace = 0;
   for(objIndex = 0; objIndex < listOfAreas.length; objIndex++)
   {
      ctx.fillRect(spaces+lastSpace - 1, 0, listOfAreas[objIndex].width*scale + 2, listOfAreas[objIndex].height*scale + 2);
      lastSpace += spaces + listOfAreas[objIndex].width*scale;
   }

   ctx.fillStyle = "#E0E0E0";

   var lastSpace = 0;
   for(objIndex = 0; objIndex < listOfAreas.length; objIndex++)
   {
      ctx.fillStyle = "#E0E0E0";
      ctx.globalAlpha = 1;
      ctx.fillRect(spaces+lastSpace, 1, listOfAreas[objIndex].width*scale, listOfAreas[objIndex].height*scale);
      ctx.textAlign = "center";
      ctx.fillStyle = "#666666";
      if(listOfAreas[objIndex].width*scale <= listOfAreas[objIndex].height*scale){
         ctx.font = 0.15*listOfAreas[objIndex].width*scale + "px Arial";   
      }
      else
      {
         ctx.font =  0.15*listOfAreas[objIndex].height*scale + "px Arial";
      }
      ctx.globalAlpha = 0.6;
      ctx.fillText(listOfAreas[objIndex].name,listOfAreas[objIndex].width*scale/2 + lastSpace + spaces,listOfAreas[objIndex].height*scale/2);

      listOfAreas[objIndex].x = spaces + lastSpace;
      listOfAreas[objIndex].y = 1;

      lastSpace += spaces + listOfAreas[objIndex].width*scale;
   }
}

function clearScreenForAreas()
{
   ctx.fillStyle = "#f5f5f5";
   ctx.globalAlpha = 1;
   ctx.fillRect(0,0,fieldWidth,0.98*fieldHeight);
}

function addTable(){
   var tableName = document.getElementById("nameOfTable").value;
   var tableWidth = document.getElementById("widthOfTable").value;
   var tableHeight = document.getElementById("heightOfTable").value;
   var areaForAddingName = document.getElementById("areaNameForTable").value;
   if(tableName == "" || tableWidth == "" || tableHeight == "" || areaForAddingName == ""){
      $('#notCompletedFields').modal('show'); 
   }
   else{
   if(tableWidth <= 0 || tableHeight <= 0){
      $('#minusOrZeroValue').modal('show');
   }
   else{
   var indexOfArea = -1;
   for(var indexForSearch = 0; indexForSearch < listOfAreas.length; indexForSearch++)
   {
      if(listOfAreas[indexForSearch].name == areaForAddingName)
      {
         indexOfArea = indexForSearch;
         break;
      }
   }

   var tableX = 0.01*listOfAreas[indexOfArea].width*scale + listOfAreas[indexOfArea].x;
   var tableY = 0.01*listOfAreas[indexOfArea].height*scale + listOfAreas[indexOfArea].y;
   //alert(tableX + " " + tableY);
   for(var indexForSearchTables = 0; indexForSearchTables < listOfTables.length; indexForSearchTables++)
   {
      if(listOfTables[indexForSearchTables].areaIndex == indexOfArea){
         tableX += listOfTables[indexForSearchTables].width;
         //tableY += listOfTables[indexForSearchTables].y*scale + 0.01*listOfAreas[indexOfArea].height*scale;
      }
   }

   var tableObject = {
      name : tableName,
      width : tableWidth,
      height : tableHeight, 
      areaIndex : indexOfArea,
      x : tableX, 
      y : tableY
   };
   listOfTables.push(tableObject);

   //alert(listOfTables[indexOfTable].x + " " + listOfTables[indexOfTable].y);

   drawTables();

   var containerForTable = document.getElementById("containerForArea" + areaForAddingName);
   var divForTable = document.createElement("div");
   var popoverId = "popover" + listOfTables[indexOfTable].name; 
   divForTable.id = "table" + listOfTables[indexOfTable].name;
   divForTable.innerHTML = "<div class=\"well well-sm form-group\">" + "<div class=\"col-xs-9\"><h6>" + listOfTables[indexOfTable].name + "</h6></div>" +
   "<button type=\"button\" class=\"btn btn-default btn-sm\" onclick=\"showPopover(" + popoverId + ")\" data-toggle=\"" + popoverId + "\"><span class=\"glyphicon glyphicon-menu-hamburger\"></span></button>";

   containerForTable.appendChild(divForTable);
   indexOfTable++;
   }
  }
}

function showPopover(id){
   $('[data-toggle="popover"]').popover();
}

function drawTables(){
   clearScreenForAreas();
   drawAreas();
   ctx.fillStyle = "#1F1F1F";
   for(var indexForDrawTables = 0; indexForDrawTables < listOfTables.length; indexForDrawTables++){
      ctx.fillRect(listOfTables[indexForDrawTables].x, listOfTables[indexForDrawTables].y, listOfTables[indexForDrawTables].width*scale, listOfTables[indexForDrawTables].height*scale);
   }
   //updateListOfTables();
}

var canvasPosition;
var startTablePositionX;
var startTablePositionY;
var scrolled = 0;
var timeHandlerForDragAndDrop;
var dragIndicator = false;
var tableIndexForDrag = 0;
var mouseClickX = 0;
var mouseClickY = 0;
areasField.onmousedown = function(){clickOnAreasField();};
areasField.onmouseup = function(){clearTimeout(timeHandlerForDragAndDrop); dragIndicator = false;};


function clickOnAreasField(){
   //alert(mouseClickX + ":" + mouseClickY);
   canvasPosition = document.getElementById("field").getBoundingClientRect();
   /*for(var k = 0; k < listOfTables.length; k++)
   {
      console.log(listOfTables[k]);
   }*/
   var positionOfCursor = getCursorPosition();
   mouseClickX = positionOfCursor.x;
   mouseClickY = positionOfCursor.y;
   //console.log(mouseClickX + ":" + mouseClickY + "..." + canvasPosition.left + ":" +canvasPosition.top);
   for(var indexForTableClick = 0; indexForTableClick < listOfTables.length; indexForTableClick++){
      if(listOfTables.length>0){
         if(listOfTables[indexForTableClick].x <= mouseClickX && listOfTables[indexForTableClick].y <= mouseClickY && 
         listOfTables[indexForTableClick].x + listOfTables[indexForTableClick].width*scale >= mouseClickX && 
         listOfTables[indexForTableClick].y + listOfTables[indexForTableClick].height*scale >= mouseClickY){      
            dragIndicator = true;
            tableIndexForDrag = indexForTableClick;
            startTablePositionX = listOfTables[tableIndexForDrag].x;
            startTablePositionY = listOfTables[tableIndexForDrag].y;
         }     
      }  
   }
}

function updateListOfTables(){
   var xRight = (listOfTables[tableIndexForDrag].x + listOfTables[tableIndexForDrag].width) > (listOfAreas[listOfTables[tableIndexForDrag].areaIndex].x + listOfAreas[listOfTables[tableIndexForDrag].areaIndex].width);
   console.log(xRight + " " + listOfTables[tableIndexForDrag].x + listOfTables[tableIndexForDrag].width + " " + (listOfAreas[listOfTables[tableIndexForDrag].areaIndex].x + listOfAreas[listOfTables[tableIndexForDrag].areaIndex].width));
   if(listOfTables[tableIndexForDrag].x < listOfAreas[listOfTables[tableIndexForDrag].areaIndex].x ||
      listOfTables[tableIndexForDrag].y < listOfAreas[listOfTables[tableIndexForDrag].areaIndex].y){
      var tableToRemove = document.getElementById("table" + listOfTables[tableIndexForDrag].name);
      if(tableToRemove){
         var containerForTableToRemove = document.getElementById("containerForArea" + listOfAreas[listOfTables[tableIndexForDrag].areaIndex].name);
         containerForTableToRemove.removeChild(tableToRemove);
      } 
   }
}

document.onmousemove = function(){
   if(dragIndicator == true){
      var positionOfCursorInOnmouseMove = getCursorPosition();
      //console.log(positionOfCursorInOnmouseMove);
      updateListOfTables();
      dragTable(positionOfCursorInOnmouseMove);
   }
}

function dragTable(p){
   listOfTables[tableIndexForDrag].x = startTablePositionX + p.x - mouseClickX;
   listOfTables[tableIndexForDrag].y = startTablePositionY + p.y - mouseClickY;
   //console.log(listOfTables[tableIndexForDrag].x + "; " + listOfTables[tableIndexForDrag].y);
   //console.log(mouseClickX + "  " + mouseClickY);
   drawTables();
}

var areasFieldPosition = areasField.getBoundingClientRect();
function getCursorPosition(){
   var xPos = event.pageX - canvasPosition.left;
   var yPos = event.pageY - scrolled - fieldHeight*0.01;
   var position = {
      x : xPos,
      y : yPos
   };
   //console.log(position.x + " " + position.y + " " + scrolled);
   return position;
}

window.onscroll = function() {
  scrolled = window.pageYOffset;
  //console.log(scrolled);
}